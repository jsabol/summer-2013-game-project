#ifndef CHARACTER_H
#define CHARACTER_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "functions.h"
#include "Timer.h"
#include <iostream>
using namespace std;

class Character {
	
	public:
	
	Character();
	
	void handle_input();

	void move();
	void move_horizontal();
	void move_vertical();
	void jump();
	void teleport(char, char);

	void draw();

	int getX();
	int getY();

	char getDir();
	bool getIsTeleporting();
	SDL_Surface* getCharFrame();

	private:

	SDL_Rect rect;
	int tpX;
	int tpY;
	char tpH;
	char tpV;
	float dx;
	float dy;
	char dir;
	int walkFrame;
	int jumpFrame;
	bool isJumping;
	bool isTeleporting;
	int tpCounter;
	bool canTeleport;
};

#endif
