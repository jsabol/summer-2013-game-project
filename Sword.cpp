#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Sword.h"
#include "Character.h"
#include <iostream>
using namespace std;

extern SDL_Surface* leftSword_l[6];
extern SDL_Surface* leftSword_r[6];
extern SDL_Surface* screen;
extern SDL_Event event;

Sword::Sword(char c) {
	side = c;
}

void Sword::handle_input() {
	bool keyDown;
	Uint8* keystates = SDL_GetKeyState(NULL);

	if(event.type == SDL_KEYDOWN) {
		keyDown = true;
	}

	if((keystates[SDLK_j] && side == 'l')
	|| (keystates[SDLK_l] && side == 'r')) {
		if(swinging == false) {
			swinging = true;
		} else if(swingTimer >= 15 && swingTimer < 30) {
			isSecondSwing = true;
			swingTimer = 100;
		}
	}

	if(swinging == true) {
		swing();
	}
}

//Swings sword, this basically determines the sprite to use
void Sword::swing() {
	if(!isSecondSwing && swingTimer < 30) {
		swingTimer++;
	} else if(swingTimer >= 100 
	&& swingTimer < 115) {
		swingTimer++;
		isSecondSwing = false;
	} else if((swingTimer >= 30 && swingTimer < 100)
	|| (swingTimer >= 115)) {
		swingTimer = 0;
		swinging = false;
		isSecondSwing = false;
	}
}

void Sword::draw(Character C) {
	if(side == 'l') {
		if(C.getDir() == 'r') {
			apply_surface(C.getX() - 16, C.getY(),
			getFrame(C), screen);
		}
		else if(C.getDir() == 'l') {
			apply_surface(C.getX(), C.getY(),
			getFrame(C), screen);
		}
	} else if(side == 'r') {
		if(C.getDir() == 'r') {
			apply_surface(C.getX() - 32, C.getY(),
			getFrame(C), screen);
		}
		else if(C.getDir() == 'l') {
			apply_surface(C.getX() - 16, C.getY(),
			getFrame(C), screen);
		}
	}
}

//Determines correct frame of sword sprite
SDL_Surface* Sword::getFrame(Character C) {
	if(C.getIsTeleporting()) return NULL;
	//If facing right
	if(C.getDir() == 'r') {
		if(swingTimer >= 0 && swingTimer < 5) {
			return leftSword_r[0];
		} else if(swingTimer >= 5 && swingTimer < 10) {
			return leftSword_r[1];
		} else if(swingTimer >= 10 && swingTimer < 15) {
			return leftSword_r[2];
		} else if(swingTimer >= 15 && swingTimer < 30) {
			return leftSword_r[3];
		} else if(swingTimer >= 100 && swingTimer < 105) {
			return leftSword_r[3];
		} else if(swingTimer >= 105 && swingTimer < 110) {
			return leftSword_r[4];
		} else if(swingTimer >= 110 && swingTimer < 115) {
			return leftSword_r[5];
		} else return NULL; //To avoid crashing if there's a glitch in the timer
	//If facing left
	} else if(C.getDir() == 'l') {
		if(swingTimer >= 0 && swingTimer < 5) {
			return leftSword_l[0];
		} else if(swingTimer >= 5 && swingTimer < 10) {
			return leftSword_l[1];
		} else if(swingTimer >= 10 && swingTimer < 15) {
			return leftSword_l[2];
		} else if(swingTimer >= 15 && swingTimer < 30) {
			return leftSword_l[3];
		} else if(swingTimer >= 100 && swingTimer < 105) {
			return leftSword_l[3];
		} else if(swingTimer >= 105 && swingTimer < 110) {
			return leftSword_l[4];
		} else if(swingTimer >= 110 && swingTimer < 115) {
			return leftSword_l[5];
		} else return NULL; //Once again, to avoid crashing
	}	
}
