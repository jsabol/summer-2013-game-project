#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <iostream>
using namespace std;

void apply_surface(int, int, SDL_Surface*, SDL_Surface*);
void handle_input();

#endif
