#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Character.h"
#include "functions.h"
#include "Timer.h"
#include <iostream>
using namespace std;

extern SDL_Surface* walking_l[10];
extern SDL_Surface* walking_r[10];
extern SDL_Surface* jumping_l[2];
extern SDL_Surface* jumping_r[2];
extern SDL_Surface* teleport_l[2];
extern SDL_Surface* teleport_r[2];
extern SDL_Surface* screen;
extern SDL_Event event;

Character::Character() {
	dx = 0;
	dy = 0;
	rect.x = 100;
	rect.y = 300;
	rect.w = 32;
	rect.h = 64;
	dir = 'r';
	walkFrame = 0;
	jumpFrame = 0;
	tpCounter = 0;
	canTeleport = true;
}

void Character::handle_input() {
	bool keyDown;
	Uint8* keystates = SDL_GetKeyState(NULL);

	if(event.type == SDL_KEYDOWN) {
		keyDown = true;
	}

	//Jump by pressing K
	if(keystates[SDLK_k]) {
		if(!isJumping && !isTeleporting) jump();
	}

	//Run left by pressing A
	if(keystates[SDLK_a]) {
		dir = 'l';
		if(dx > -6) dx -= 0.25;
	//Run right by pressing D
	} else if(keystates[SDLK_d]) {
		dir = 'r';
		if(dx < 6) dx += 0.25;
	//Slow down if not pressing anything
	} else if(!keyDown) {
		if(dx > 0 && !isJumping) dx -= 0.125;
		if(dx < 0 && !isJumping) dx += 0.125;
	}

	move();
	if(!isJumping) canTeleport = true;

	//Teleport by pressing I
	if(keystates[SDLK_i] && canTeleport) {
		if(keystates[SDLK_w]) {
			if(keystates[SDLK_a]) {
				teleport('l', 'u');
			} else if(keystates[SDLK_d]) {
				teleport('r', 'u');
			} else {
				teleport('n', 'u');
			}
		} else if(keystates[SDLK_a]) {
			teleport('l', 'n');
		} else if(keystates[SDLK_d]) {
			teleport('r', 'n');
		} else teleport('n', 'n');
	}
}

void Character::move() {
	//Make sure jump mode is on if not on floor
	if(rect.y > 300) isJumping = true;
	//Handle movement if teleporting
	if(isTeleporting) {
		//Draw teleport sprite
		if(tpCounter == 0 || tpCounter == 2) {
			if(dir == 'r') {
				apply_surface(rect.x, rect.y,
				teleport_r[0], screen);
			} else if(dir == 'l') {
				apply_surface(rect.x, rect.y,
				teleport_l[0], screen);
			}
		} else if(tpCounter == 1 || tpCounter == 4) {
			if(dir == 'r') {
				apply_surface(rect.x, rect.y,
				teleport_r[1], screen);
			} else if(dir == 'l') {
				apply_surface(rect.x, rect.y,
				teleport_l[1], screen);
			}
		//Actual teleportation
		} else if(tpCounter == 15) {
			if(tpV == 'u') {
				if(tpH == 'l') {
					rect.x -= 50;
					rect.y -= 50;
				} else if(tpH == 'r') {
					rect.x += 50;
					rect.y -= 50;
				} else if(tpH == 'n') {
					rect.y -= 70;
				}
			} else if(tpV == 'n') {
				if(tpH == 'l') {
					rect.x -= 70;
				} else if(tpH == 'r') {
					rect.x += 70;
				}
			}
		//Redraw teleport sprite
		} else if(tpCounter == 16 || tpCounter == 18) {
			if(dir == 'r') {
				apply_surface(rect.x, rect.y,
				teleport_r[0], screen);
			} else if(dir == 'l') {
				apply_surface(rect.x, rect.y,
				teleport_l[0], screen);
			}
		} else if(tpCounter == 17 || tpCounter == 19) {
			if(dir == 'r') {
				apply_surface(rect.x, rect.y,
				teleport_r[1], screen);
			} else if(dir == 'l') {
				apply_surface(rect.x, rect.y,
				teleport_l[1], screen);
			}
		//Reset teleport timer, teleport is off
		} else if(tpCounter == 20) {
			tpCounter = 0;
			isTeleporting = false;
			//Reset momentum
			dx = 0;
			dy = 0;
		}
		tpCounter++;
	} else {
		move_horizontal();
		move_vertical();
	}
}

void Character::move_horizontal() {
	rect.x += dx;
}

//Vertical movement
void Character::move_vertical() {
	int absDy; //Absolute value of dy
	if(dy < 0) absDy = -1*dy; //Perform absolute value
	else absDy = dy;
	if(isJumping) {
		//Move using increments to check
		//collision every time
		for(int i = 0; i < absDy; i++) { 
			if(dy < 0) rect.y--;
			else if(dy > 0) rect.y++;
			//Check if y is 300
			if(rect.y == 300) {
				isJumping = false; //Not jumping
				jumpFrame = 0;
				dy = 0;
			}
		}
	}
	if(isJumping) dy++; //Effect of gravity
}

void Character::jump() {
	isJumping = true;
	dy = -15;
}

void Character::teleport(char h, char v) {
	isTeleporting = true;
	canTeleport = false;
	if(v == 'u') isJumping = true;
	tpH = h;
	tpV = v;
}

void Character::draw() {
	apply_surface(rect.x, rect.y, getCharFrame(), screen);
}

int Character::getX() {
	return rect.x;
}

int Character::getY() {
	return rect.y;
}

char Character::getDir() {
	return dir;
}

bool Character::getIsTeleporting() {
	return isTeleporting;
}

//Returns the correct sprite to render depending on position
SDL_Surface* Character::getCharFrame() {
	//Is teleporting
	if(isTeleporting) return NULL;
	//Facing right
	if(dir == 'r') {
		//Is jumping
		if(isJumping) {
			if(jumpFrame == 0) {
				jumpFrame = 1;
				return jumping_r[0];
			}
			if(jumpFrame == 1) {
				return jumping_r[1];
			}
		}
		//Is on ground
		//Moving forward
		if(dx > 0) {
			walkFrame++;
			if(walkFrame == 10) walkFrame = 0;
			return walking_r[walkFrame];
		//Not moving
		} else if(dx == 0) {
			walkFrame = 0;
			return walking_r[walkFrame];
		//Moving backward(sliding)
		} else if(dx < 0) {
			walkFrame = 0;
			return walking_r[0];
		}
	//Facing left
	} else if(dir == 'l') {
		//Is jumping
		if(isJumping) {
			if(jumpFrame == 0) {
				jumpFrame = 1;
				return jumping_l[0];
			}
			if(jumpFrame == 1) {
				return jumping_l[1];
			}
		}
		//Is on ground
		//Moving forward
		if(dx < 0) {
			walkFrame++;
			if(walkFrame == 10) walkFrame = 0;
			return walking_l[walkFrame];
		//Not moving
		} else if(dx == 0) {
			walkFrame = 0;
			return walking_l[walkFrame];
		//Moving backward(sliding)
		} else if(dx > 0) {
			walkFrame = 0;
			return walking_l[0];
		}
	}
}
