#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "functions.h"
#include "Timer.h"
#include "Character.h"
#include "Sword.h"
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

const int framerate = 60;

SDL_Event event;
int speed = 50;

int frame = 0;
Timer fps;
Timer update;

//Load images
//Character
SDL_Surface* walking_l[10] = {NULL};
SDL_Surface* walking_r[10] = {NULL};
SDL_Surface* jumping_l[2] = {NULL};
SDL_Surface* jumping_r[2] = {NULL};
SDL_Surface* teleport_l[2] = {NULL};
SDL_Surface* teleport_r[2] = {NULL};
//Swords
SDL_Surface* leftSword_l[6] = {NULL};
SDL_Surface* leftSword_r[6] = {NULL};

SDL_Surface* background = NULL;
//Load screen
SDL_Surface* screen = NULL;

bool quit = false;
int charFrame = 0;
int x = 0;

Character myCharacter;
Sword leftSword('l');
Sword rightSword('r');

int main(int argc, char* argv[]) {
	
	//Initialize SDL
	SDL_Init(SDL_INIT_EVERYTHING);
	//Set up screen
	screen = SDL_SetVideoMode(640, 480, 32, SDL_SWSURFACE);

	//Assign sprites
	walking_l[0] = IMG_Load("Sprites/walking_0_l.png");
	walking_l[1] = IMG_Load("Sprites/walking_1_l.png");
	walking_l[2] = IMG_Load("Sprites/walking_2_l.png");
	walking_l[3] = IMG_Load("Sprites/walking_3_l.png");
	walking_l[4] = IMG_Load("Sprites/walking_4_l.png");
	walking_l[5] = IMG_Load("Sprites/walking_5_l.png");
	walking_l[6] = IMG_Load("Sprites/walking_6_l.png");
	walking_l[7] = IMG_Load("Sprites/walking_7_l.png");
	walking_l[8] = IMG_Load("Sprites/walking_8_l.png");
	walking_l[9] = IMG_Load("Sprites/walking_9_l.png");
	walking_r[0] = IMG_Load("Sprites/walking_0_r.png");
	walking_r[1] = IMG_Load("Sprites/walking_1_r.png");
	walking_r[2] = IMG_Load("Sprites/walking_2_r.png");
	walking_r[3] = IMG_Load("Sprites/walking_3_r.png");
	walking_r[4] = IMG_Load("Sprites/walking_4_r.png");
	walking_r[5] = IMG_Load("Sprites/walking_5_r.png");
	walking_r[6] = IMG_Load("Sprites/walking_6_r.png");
	walking_r[7] = IMG_Load("Sprites/walking_7_r.png");
	walking_r[8] = IMG_Load("Sprites/walking_8_r.png");
	walking_r[9] = IMG_Load("Sprites/walking_9_r.png");
	
	jumping_l[0] = IMG_Load("Sprites/jumping_0_l.png");
	jumping_l[1] = IMG_Load("Sprites/jumping_1_l.png");
	jumping_r[0] = IMG_Load("Sprites/jumping_0_r.png");
	jumping_r[1] = IMG_Load("Sprites/jumping_1_r.png");
	
	teleport_l[0] = IMG_Load("Sprites/teleport_0_l.png");
	teleport_l[1] = IMG_Load("Sprites/teleport_1_l.png");
	teleport_r[0] = IMG_Load("Sprites/teleport_0_r.png");
	teleport_r[1] = IMG_Load("Sprites/teleport_1_r.png");

	leftSword_l[0] = IMG_Load("Sprites/leftSword_0_l.png");
	leftSword_l[1] = IMG_Load("Sprites/leftSword_1_l.png");
	leftSword_l[2] = IMG_Load("Sprites/leftSword_2_l.png");
	leftSword_l[3] = IMG_Load("Sprites/leftSword_3_l.png");
	leftSword_l[4] = IMG_Load("Sprites/leftSword_4_l.png");
	leftSword_l[5] = IMG_Load("Sprites/leftSword_5_l.png");
	leftSword_r[0] = IMG_Load("Sprites/leftSword_0_r.png");
	leftSword_r[1] = IMG_Load("Sprites/leftSword_1_r.png");
	leftSword_r[2] = IMG_Load("Sprites/leftSword_2_r.png");
	leftSword_r[3] = IMG_Load("Sprites/leftSword_3_r.png");
	leftSword_r[4] = IMG_Load("Sprites/leftSword_4_r.png");
	leftSword_r[5] = IMG_Load("Sprites/leftSword_5_r.png");

	//Background
	background = IMG_Load("Sprites/testBackground.png");
	
	//Main loop
	while(!quit) {
		//Start framerate regulation timer
		fps.start();
		apply_surface(0, 0, background, screen);
		//Wait for event
		while(SDL_PollEvent(&event)) {
			//If user X'es out the window, quit
			if(event.type == SDL_QUIT) {
				quit = true;
				cout << "quit" << endl;
			}
		}	

		myCharacter.handle_input();
		leftSword.handle_input();
		rightSword.handle_input();
		//Update sprites
		myCharacter.draw();
		leftSword.draw(myCharacter);
		rightSword.draw(myCharacter);
		//Update screen
		SDL_Flip(screen);
		frame++;
		//Frame delay
		if(fps.get_ticks() <
		1000/framerate) {
			SDL_Delay((1000/framerate) -
			fps.get_ticks());
		}
	}

	//Quit SDL
	SDL_Quit();

	return 0;
}
