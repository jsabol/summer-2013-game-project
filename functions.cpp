#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "functions.h"
#include <iostream>
using namespace std;

extern SDL_Event event;
extern int speed;

void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination) {
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;
	SDL_BlitSurface(source, NULL, destination, &offset);
}

void handle_input() {
	bool keyDown;
	Uint8* keystates = SDL_GetKeyState(NULL);

	if(event.type == SDL_KEYDOWN) {
		keyDown = true;
	}

	if(keystates[SDLK_LEFT] && keyDown && event.key.keysym.sym == SDLK_LEFT) {
		if(speed > 0) speed -= 10;
	}
	
	if(keystates[SDLK_RIGHT] && keyDown && event.key.keysym.sym == SDLK_RIGHT) {
		speed += 10;
	}
}
