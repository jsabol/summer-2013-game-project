all: game

game: game.o functions.o Timer.o Character.o Sword.o
	g++ game.o functions.o Timer.o Character.o Sword.o -o game -lSDL -lSDL_image

game.o: game.cpp functions.h Timer.h Character.h
	g++ -c game.cpp

functions.o: functions.cpp functions.h
	g++ -c functions.cpp

Timer.o: Timer.cpp Timer.h
	g++ -c Timer.cpp

Character.o: Character.cpp Character.h Timer.h
	g++ -c Character.cpp

Sword.o: Sword.cpp Sword.h Character.h
	g++ -c Sword.cpp

clean:
	rm -f *.o game
