#ifndef SWORD_H
#define SWORD_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Character.h"
#include <iostream>
using namespace std;

class Sword {
	
	public:

	Sword(char);

	void handle_input();

	void swing();

	void draw(Character);

	SDL_Surface* getFrame(Character);

	private:

	char side;
	int swingTimer;
	bool swinging;
	bool isSecondSwing;
};

#endif
